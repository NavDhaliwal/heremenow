//TODO: get current location
// show markers
//efficient way to store events nearby. Ex: (45.504654, -73.563960) to (45.504895, -73.571641) is good square.
//HashMap -> key: (centre location), value: [List of events in that square]
//set up DB
//

let Welcome = React.createClass({
        
    render: function() {
        
        return (
            
            <div >
            <TopHeading/>
            <EventScroller/>
            <ReactBootstrap.Panel bsStyle="info">
                {/*<ReactBootstrap.Grid>*/}
                    <ReactBootstrap.Row>
                        
                        <ReactBootstrap.Col xs={4} sm={2} md={2} lg={2}>
                            <Tags/>
                        </ReactBootstrap.Col>
                        
                        <ReactBootstrap.Col xs={8} sm={10} md={10} lg={10} >
                            <GMap2 />
                        </ReactBootstrap.Col>

                    </ReactBootstrap.Row>
                {/*</ReactBootstrap.Grid>*/}
            </ReactBootstrap.Panel>
            </div>
            );
    }
});


///////////////////////////////////////////////////////////////////////////////////////////////////////
let TopHeading = React.createClass({
    
    render: function() {
        let titleText =  <ReactBootstrap.PageHeader>Here Me Now</ReactBootstrap.PageHeader>;
        return (
            <div>
            <ReactBootstrap.Col>
            <ReactBootstrap.Panel bsStyle="primary" header={titleText}>
            </ReactBootstrap.Panel>
            </ReactBootstrap.Col>
            </div>
        );
    }
});
///////////////////////////////////////////////////////////////////////////////////////////////////////
let FeaturedEvent = React.createClass({
    
    render: function(){
        return(
        <div style={this.props.style}>
            <center>
                <ReactBootstrap.Image src={this.props.Image} style={this.props.eventImgStyle} />
                <h3>{this.props.description}</h3>
            </center>
        </div>
        );
    }   
});


let EventScroller = React.createClass({
    
    getInitialState: function() {
        return {
            windowHeight: window.innerHeight,
            windowWidth: window.innerWidth,
            activeIndex: 0,
            backgroundColors: ['#C9FFE5','#7CB9E8','#B184BD','#FF9A66','#FDED01','#F1FFFF','#FF91AE','#C0FF00','#00CC99','#7E1734'],
            eventList: [],
            totalItems: 10
        };
    },

    handleResize: function(e) {
        this.setState({windowHeight: window.innerHeight, windowWidth: window.innerWidth});
    },

    componentDidMount: function() {
        window.addEventListener('resize', this.handleResize);
    },

    componentWillMount: function(){
        

        

        
        
        let eventList=[];
        for(var i=0;i<this.state.totalItems;i++)
        {
            const Tmp=({
                //style: eventCarouselStyleList[i], 
                description: 'Event1',
                Image: '/assets/comedy.jpg', 
                //eventImgStyle: eventImgStyle
            });
            eventList.push(Tmp);
        }
        this.setState({eventList: eventList});
    },

    componentWillUnmount: function() {
        window.removeEventListener('resize', this.handleResize);
    },

    render: function(){
        const eventCarouselStyle = ({
        width: this.state.windowWidth,
        height: this.state.windowHeight/3,
        //'overflowY' : 'hidden'
        });

        const eventImgStyle = ({
        //width: this.state.windowWidth/6,
        height: this.state.windowHeight/6,
        //'overflowY' : 'hidden'
        });

        let eventCarouselStyleList=[];
        for(var i=0;i<this.state.totalItems;i++)
        {
            const eventCarouselStyleTmp=({
                'background-color': this.state.backgroundColors[i],
                width: this.state.windowWidth,
                height: this.state.windowHeight/3,
                'overflowY' : 'hidden'
            });
            eventCarouselStyleList.push(eventCarouselStyleTmp);
        }
        
         return (
            <div>
                <ReactBootstrap.Carousel style={eventCarouselStyle} interval={5000} onSelect={this.activeItemChanged}>

                {this.state.eventList.map(function(obj,index){
                    return <ReactBootstrap.Carousel.Item onClick={this.featuredEventSelected}> 
                    <FeaturedEvent style={eventCarouselStyleList[index]} description={obj.description} Image={obj.Image} eventImgStyle={eventImgStyle}/>
                    </ReactBootstrap.Carousel.Item>;
                  })}
                
                    
                

                </ReactBootstrap.Carousel>
            </div>
      
         );
    },
    activeItemChanged: function(e){
        this.setState({activeIndex: e});
    },
    featuredEventSelected: function(e){
        alert(this.state.activeIndex);
    }   
});
///////////////////////////////////////////////////////////////////////////////////////////////////////

const tooltips = ({
 comedy: <ReactBootstrap.Tooltip id="tooltip">Comedy Events</ReactBootstrap.Tooltip>,
 music: <ReactBootstrap.Tooltip id="tooltip">Music Events</ReactBootstrap.Tooltip>,
 movies: <ReactBootstrap.Tooltip id="tooltip">Movies Events</ReactBootstrap.Tooltip>,
 drinks: <ReactBootstrap.Tooltip id="tooltip">Drinks Events</ReactBootstrap.Tooltip>,
 food: <ReactBootstrap.Tooltip id="tooltip">Food Events</ReactBootstrap.Tooltip>,
 all: <ReactBootstrap.Tooltip id="tooltip">All Events</ReactBootstrap.Tooltip>,
}
);


//Tags component contains the event tags(images) which can be activated by clicking and a search bar
let Tags = React.createClass({
    
    
    getInitialState: function() {
        return{
            comedySelected : false,
            comedyImgPath : '/assets/comedy.jpg',
            drinksSelected : false,
            drinksImgPath : '/assets/drinks.jpg',
            musicSelected : false,
            musicImgPath : '/assets/music.jpg',
            moviesSelected : false,
            moviesImgPath : '/assets/movies.jpg',
            foodSelected : false,
            foodImgPath : '/assets/food.jpg',
            allSelected : false,
            allImgPath : '/assets/all.jpg'
            };
        },
    render: function() {
        let Button  = ReactBootstrap.Button;
        let Image  = ReactBootstrap.Image;
        let Col  = ReactBootstrap.Col;
        this.imagesSetStatus();

        return (<div>
                        
                    
                    <ReactBootstrap.Panel bsStyle="info" header={<h1>Events</h1>}>
                        <SearchTags/>

                        <ReactBootstrap.OverlayTrigger overlay={tooltips.all}>
                            <Image src={this.state.allImgPath} rounded responsive onClick={this.allClicked}/>
                        </ReactBootstrap.OverlayTrigger>

                        <ReactBootstrap.OverlayTrigger overlay={tooltips.comedy}>
                            <Image src={this.state.comedyImgPath} rounded responsive onClick={this.comedyClicked}/>
                        </ReactBootstrap.OverlayTrigger>

                        <ReactBootstrap.OverlayTrigger overlay={tooltips.music}>
                            <Image src={this.state.musicImgPath} rounded responsive onClick={this.musicClicked}/>
                        </ReactBootstrap.OverlayTrigger>

                        <ReactBootstrap.OverlayTrigger overlay={tooltips.drinks}>
                            <Image src={this.state.drinksImgPath} rounded responsive onClick={this.drinksClicked}/>
                        </ReactBootstrap.OverlayTrigger>

                        <ReactBootstrap.OverlayTrigger overlay={tooltips.movies}>
                            <Image src={this.state.moviesImgPath} rounded responsive onClick={this.moviesClicked}/>
                        </ReactBootstrap.OverlayTrigger>

                        <ReactBootstrap.OverlayTrigger overlay={tooltips.food}>
                            <Image src={this.state.foodImgPath} rounded responsive onClick={this.foodClicked}/>
                        </ReactBootstrap.OverlayTrigger>

                    
                    </ReactBootstrap.Panel>
                    

                    
                </div>);
    },
    imagesSetStatus: function(){
        this.comedyImagesSetStatus();
        this.musicImagesSetStatus();
        this.drinksImagesSetStatus();
        this.moviesImagesSetStatus();
        this.foodImagesSetStatus();
        this.allImagesSetStatus();
    },
    comedyImagesSetStatus: function(){
        
        if(this.state.comedySelected)
        {
            this.state.comedyImgPath = '/assets/comedySel.jpg'
        }
        else
        {
            this.state.comedyImgPath = '/assets/comedy.jpg'
        }
    },
    musicImagesSetStatus: function(){
        
        if(this.state.musicSelected)
        {
            this.state.musicImgPath = '/assets/musicSel.jpg'
        }
        else
        {
            this.state.musicImgPath = '/assets/music.jpg'
        }
    },
    drinksImagesSetStatus: function(){
        
        if(this.state.drinksSelected)
        {
            this.state.drinksImgPath = '/assets/drinksSel.jpg'
        }
        else
        {
            this.state.drinksImgPath = '/assets/drinks.jpg'
        }
    },
    moviesImagesSetStatus: function(){
        
        if(this.state.moviesSelected)
        {
            this.state.moviesImgPath = '/assets/moviesSel.jpg'
        }
        else
        {
            this.state.moviesImgPath = '/assets/movies.jpg'
        }
    },
    foodImagesSetStatus: function(){
        
        if(this.state.foodSelected)
        {
            this.state.foodImgPath = '/assets/foodSel.jpg'
        }
        else
        {
            this.state.foodImgPath = '/assets/food.jpg'
        }
    },
    allImagesSetStatus: function(){
        
        if(this.state.allSelected)
        {
            this.state.allImgPath = '/assets/all.jpg'
        }
        else
        {
            this.state.allImgPath = '/assets/none.jpg'
        }
    },
    comedyClicked : function(e){
       this.setState({comedySelected: !this.state.comedySelected, allSelected: false});
    },
    musicClicked : function(e){
       this.setState({musicSelected: !this.state.musicSelected, allSelected: false});
    },
    drinksClicked : function(e){
       this.setState({drinksSelected: !this.state.drinksSelected, allSelected: false});
    },
    moviesClicked : function(e){
       this.setState({moviesSelected: !this.state.moviesSelected, allSelected: false});
    },
    foodClicked : function(e){
       this.setState({foodSelected: !this.state.foodSelected, allSelected: false});
    },
    allClicked : function(e){
       if(!this.state.allSelected)
       {
           this.setState({
                allSelected: true,
                comedySelected: true,
                musicSelected: true,
                drinksSelected: true,
                moviesSelected: true,
                foodSelected: true});
       }
       else{
           
           this.setState({
                allSelected: false,
                comedySelected: false,
                musicSelected: false,
                drinksSelected: false,
                moviesSelected: false,
                foodSelected: false});
       
       }
    }

    
});

let SearchTags = React.createClass({
  getInitialState: function() {
    return {
      value: ''
    };
  },

  getValidationState: function() {
  },

  handleChange: function(e) {
    this.setState({ value: e.target.value });
    //TODO: Search in DB & change in map
  },

  render: function() {
    return (
      <form>
        <ReactBootstrap.FormGroup controlId="formBasicText" validationState={this.getValidationState()}>
          
          <ReactBootstrap.FormControl type="text" value={this.state.value} placeholder="Search Events" onChange={this.handleChange}/>
          
        </ReactBootstrap.FormGroup>
      </form>
    );
  }
});




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


let GMap2 = React.createClass({

    getInitialState: function() {

        return {windowHeight: window.innerHeight};
  },

  handleResize: function(e) {
    this.setState({windowHeight: window.innerHeight});
  },


  componentDidMount: function() {
        window.addEventListener('resize', this.handleResize);
        

        //load the maps-API asynchronously  
        window.addEventListener(
        'load',
        function(){
        loadScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyAscEc9TLPrO_P8RPOJBkCddf_43QBM4GY&callback=init')
        },
        false);
  },
  
  componentWillUnmount: function() {
    window.removeEventListener('resize', this.handleResize);
  },
  render: function(){
        const mapStyle = {
        height: this.state.windowHeight,
        border: '1px solid black'
        };
        return(
            <div>
                <div id="map" style={mapStyle}>Loading...</div>
            </div>
        )}
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ARC_DE_TRIOMPHE_POSITION = {
  lat: 48.873947,
  lng: 2.295038
};

const EIFFEL_TOWER_POSITION = {
  lat: 48.858608,
  lng: 2.294471
};
let initialCenter = EIFFEL_TOWER_POSITION;
/////////////////////////////////////////////////////////////////////////////////////////////////////////

function loadScript(url,callback){
    var script = document.createElement('script');
    script.setAttribute('type','text/javascript');
    if(typeof callback==='function'){
    script.addEventListener('load',callback,false);
    }
    script.setAttribute('src',url);
    document.body.appendChild(script);
}

//callback-function for load of maps-API
function init(){
    /*alert(['init',
        '---------',
        'google.maps->'+typeof google.maps,
        'GMaps->'+typeof window.GMaps 
        ].join('\n'));*/
    
    //callback-function for load of GMaps-library
    init2=function(){
    /*alert(['init2',
        '---------',
        'google.maps->'+typeof google.maps,
        'GMaps->'+typeof window.GMaps 
        ].join('\n'));*/
    new GMaps({
        el: '#map',
        div: '#map',
        lat: EIFFEL_TOWER_POSITION.lat,
        lng: EIFFEL_TOWER_POSITION.lng,
        zoom: 15,
        click: function(e) {
            alert('You clicked on the map');
        },
        zoom_changed: function(e) {
            alert('You zoomed the map');
        },
        drag: function(e){
            //alert('dragged');
        }
    });
    }
    //load GMaps
    loadScript('/assets/gmaps.js',init2);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



