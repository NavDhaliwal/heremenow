* Rails version: 5.0.1

* System dependencies: React, ReactBootstrap

HereMeNow will provide a simple event search near the user's location, who might not have planned for an activity (although you can see future events too), and want to know what is happening in their neighborhood "now". 

User: "I am here now so show me what is happening near me."

We can expand to use this for not just fun local activities but also for emergency situations where a person needs assistance at a particular location and the emergency services are able to monitor and lookup the precise location and dispatch assistance or contact the person directly. 

This is a web app for looking up present or near future events of different genres through a google map based interface. User can create/edit events for other people to lookup and attend.

The 'HereMeNow1.png' and 'HereMeNow2.png' shows the basic UI which will be updated as I go along developing this app. I used ReactBootstrap gem for RoR extensively. The UI is responsive for all screen resolutions and dimensions.

The carousel on top will show top 10 featured events (sponsored/popular).

The Events column on the left helps in searcing a particular event or a genre of events through toggling the event genre represented by images. If the image of an event category is colored, we will show markers on the map for that event type.
If the event category is unselected, the image representing the category goes black and white.

The map will show the event markers for an event or event type/s.



TODO:

* Ability to add/edit events.

* Show markers for events.

* The events column is updated depending the recent searches based on categories. So if a person always looks up music events, that will be the top event category automatically.

* Sign Up/Login ability to save events or connect with other people. (This feature is optional as generally users do not like to signup to use a simple tool to search events near them).

* Switch to Redux for all the data handling.